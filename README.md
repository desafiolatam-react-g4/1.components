EJERCICIO 1: COMPONENTES
---
Se deben desarrollar los siguientes componentes: 

### `<TabsBlock />`
- Cambiar contenido de un bloque mediante un menú
- Contenido del bloque puede ser de cualquier tipo
- Soporte de configuraciones por propiedades

### `<CollapseBlock />`
- Expandir y colapsar verticalmente un bloque mediante una cabecera
- Contenido del bloque puede ser de cualquier tipo
- Soporte de configuraciones por propiedades

### `<Switch />`
- Manejar estados `on` y `off` (`true` y `false`)
- Soporte de configuraciones por propiedades
- Soporte valor de forma controlada

### `<Input />`
- Soporte de elementos `input` de tipo `text` y `number` según sea el caso
- Soporte de configuraciones por propiedades
- Soporte de valor de forma controlada

### `<Autocomplete />`
- Autocompletar texto escrito en componente `<Input />` de tipo `text`
- Soporte de configuraciones por propiedades

### `<Notification />`
- Visualizar notificaciones de tipo `success`, `info`, `error` y `warning`
- Contenido puede ser de cualquier tipo
- Soporte de configuraciones por propiedades
- Crear sistema de notificationes centralizado

### `<Dropdown />`
- Selección de una opción
- Soporte de valor de forma controlada
- Soporte de configuraciones por propiedades
- Selección de multiples opciones

### `<Modal />`
- Visualizar contenido en una ventana modal
- Contenido puede ser de cualquier tipo
- Soporte de configuraciones por propiedades
- Crear sistema de ventanas modales centralizado

## Ejemplo de resultado
[Ir a ejemplo](https://desafiolatam-1-components.firebaseapp.com/)

## Recursos
- [Documentación oficial de React.js](https://reactjs.org/docs)
- [Repositorio oficial de Create React App](https://github.com/facebook/create-react-app)

***
© [DesafioLatam](https://desafiolatam.com) - Todos los derechos reservados
